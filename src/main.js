import './style.css'
import { Elm } from "./Main.elm"

async function main () {
    const root = document.querySelector('#app')
    const flags = {}
    const app = Elm.Main.init({ node: root, flags: flags })

    window.addEventListener("popstate", function (_) {
	// handle local path changes due to user's Back / Forward commands
	app.ports.pathChanged.send(location.pathname)
    })

    window.addEventListener("click", function (event) {
	const { target } = event
	const href = target ? target.href : null

	if (href && href.startsWith(location.origin)) {
	    // It's a clicked link and the href is a path local to the
	    // document. Add it to history and notify the Elm side; and do nothing else.
	    const path = href.slice(location.origin.length)
	    history.pushState('', '', path)
	    app.ports.pathChanged.send(path)

	    event.preventDefault()
	    event.stopPropagation()
	}
    })

    // Notify Elm side of initial path
    app.ports.pathChanged.send(location.pathname)
}

main()
