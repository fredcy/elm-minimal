port module Main exposing (..)

import Browser
import Html exposing (..)
import Html.Attributes as HA exposing (..)
import Html.Events as HE exposing (..)
import Json.Decode as JD


port pathChanged : (String -> msg) -> Sub msg


type Msg
    = PathChanged String


type alias Model =
    { path : String
    }


modelDefault : Model
modelDefault =
    { path = ""
    }


main : Program JD.Value Model Msg
main =
    Browser.element
        { init = init
        , update = update
        , subscriptions = subscriptions
        , view = view
        }


init : JD.Value -> ( Model, Cmd Msg )
init _ =
    ( modelDefault, Cmd.none )


update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    case msg of
        PathChanged path ->
            ( { model | path = path }, Cmd.none )


subscriptions : Model -> Sub Msg
subscriptions _ =
    pathChanged PathChanged


view : Model -> Html Msg
view model =
    div [ class "app", id "app" ]
        [ viewNav
        , case model.path of
            "/" ->
                viewHome

            "/help" ->
                viewHelp

            "/about" ->
                viewAbout

            _ ->
                view404
        ]


viewNav : Html Msg
viewNav =
    div []
        [ ul []
            [ li [] [ a [ href "/" ] [ text "home" ] ]
            , li [] [ a [ href "/help" ] [ text "help" ] ]
            , li [] [ a [ href "/about" ] [ text "about" ] ]
            ]
        ]


viewHome : Html Msg
viewHome =
    div []
        [ h1 [] [ text "Home" ]
        , text "This is the home page"
        ]


viewHelp : Html Msg
viewHelp =
    div []
        [ h1 [] [ text "Help" ]
        , text "This is the help page"
        ]


viewAbout : Html Msg
viewAbout =
    div []
        [ h1 [] [ text "About" ]
        , text "This is the about page"
        ]


view404 : Html Msg
view404 =
    div []
        [ h1 [] [ text "404" ]
        , text "This is the 404 page"
        ]
