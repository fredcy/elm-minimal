# Elm Minimal App

This provides a minimal Elm application that

+ is built using the [Vite](https://vitejs.dev/) tool (via [vite-plugin-elm](https://github.com/hmsk/vite-plugin-elm)), and

+ does some simplistic SPA routing without using Elm's [Browser.application framework](https://guide.elm-lang.org/webapps/navigation.html).

## Install

```
npm install
```

## Run in development with live reloading

```
npm run dev
```

Visit displayed URL in browser (http://localhost:3000/ by default).

## Build for production

```
npm run build
```

## Test production build

```
python -m http.server -d dist
```

Visit http://0.0.0.0:8000/ in browser.

## nginx configuration

For routing to work on page load or reload with paths other than the root ("/"), the HTTP server needs to route all such paths to index.html.  For nginx a `location` block like this can suffice:

```
location {
    try_files $uri $uri/ /index.html?args;
}
```

With that, static assets like style.css will be served by the corresponding file (because of the `$uri` part) and everything else will be served by index.html.

Note that with this, nginx will never return a true 404 (not found) HTTP response for any location since index.html is the fallback for all paths.
